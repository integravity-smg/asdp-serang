module.exports = (grunt) ->

  require("load-grunt-tasks") grunt,
    scope: "devDependencies"

  require("time-grunt") grunt

  grunt.initConfig

    pkg: grunt.file.readJSON("package.json")